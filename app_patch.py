import boto3
import re
import logging
from enum import Enum

client = boto3.client('s3', region_name='ap-southeast-1')
s3 = boto3.resource('s3')
bucket = "cdn-sg-qa-secure-origin.ampostech.com"  # replace with bucket name

logger = logging.getLogger()
logger.setLevel(logging.INFO)
log_format = '%(asctime)s: %(message)s'
logging.basicConfig(level=logging.INFO, format=log_format)


# in case writing to file
# logging.basicConfig(filename='patch_metadata_s3.log', filemode='w', level=logging.INFO, format=log_format)


class SystemDefined(Enum):
    cacheControl = "CacheControl"
    contentDisposition = "ContentDisposition"
    contentEncoding = "ContentEncoding"
    contentLanguage = "ContentLanguage"
    contentType = "ContentType"
    expired = "Expires"


def patch_metadata_s3(url, new_url, failed_key):
    key = get_key(url)
    new_key = get_key(new_url)
    try:
        required_patch = False

        response = client.head_object(
            Bucket=bucket,
            Key=key
        )['ResponseMetadata']['HTTPHeaders']

        logger.info("response: {}".format(response))
        # print("response['content-type']: {}".format(response['content-type']))
        # print("response['content-disposition']: {}".format(response['content-disposition']))

        content_type = response['content-type']
        content_disposition = response['content-disposition']
        cache_control = response['cache-control']

        search_content = re.search(r'(charset=utf-8|charset=UTF-8)', content_type)
        if not search_content or search_content == '':
            logger.info("Required Patch ContentType on key: {}, {}".format(key, content_type))
            if re.search(r';$', content_type):
                content_type = content_type + "charset=utf-8;"
            else:
                content_type = content_type + ";" + "charset=utf-8;"
            required_patch = True

        search_disposition = re.search(r'filename\*=utf-8', content_disposition)
        if not search_disposition or search_disposition == '':
            logger.info("Required Patch ContentDisposition on key: {}, {}".format(key, content_disposition))
            file_name = get_file_name(response['content-disposition'])
            if re.search(r';$', content_disposition):
                content_disposition = content_disposition + "filename*=utf-8''" + file_name + ";"
            else:
                content_disposition = content_disposition + ";filename*=utf-8''" + file_name + ";"
            required_patch = True

        # logger.info('Content-Type: {}'.format(content_type))
        # logger.info('Content-Disposition: {}'.format(content_disposition))

        meta_data = {
            SystemDefined.contentType: content_type,
            SystemDefined.cacheControl: cache_control,
            SystemDefined.contentDisposition: content_disposition
        }
        logger.info("Metadata: {}".format(meta_data))

        if required_patch:
            upload_result = upload_file(key, new_key, meta_data)
            if not upload_result:
                failed_key.append({"Key": key, "Metadata": meta_data})
                logger.warning("error on patching key: {}".format(key))

    except Exception as e:
        logger.error("[MAIN ERROR]: {}".format(e.args))


def get_key(url: str):
    return url.split('/', 2)[-1].split('/', 1)[1]


def get_file_name(file_name: str):
    name = re.search('(?<=filename=)(.*)', file_name).groups()[0]
    return name


def upload_file(file_name: str, new_file_name: str, meta_data: dict):
    try:
        response = client.copy_object(Bucket=bucket,
                                      Key=new_file_name,
                                      CopySource={"Bucket": bucket, "Key": file_name},
                                      CacheControl=meta_data[SystemDefined.cacheControl],
                                      ContentDisposition=meta_data[SystemDefined.contentDisposition],
                                      ContentType=meta_data[SystemDefined.contentType],
                                      MetadataDirective='REPLACE')

        # client.delete_object(Bucket=bucket, Key=file_name)

        logger.debug('Response: {}'.format(response))
    except Exception as e:
        logger.error("[ERROR] upload_file: {}".format(e.args))
        return False

    return True


if __name__ == '__main__':
    not_patch = "https://cdn-sg-qa-secure.ampostech.com/ampos1-qa/announcement/e9124415-1392-4a80-81d8-d6bab6125e02/original.f07d1e26-f9f0-4563-b201-09588a64ed19.pdf"
    patch = "https://cdn-sg-qa-secure.ampostech.com/ampos1-qa/announcement/e9124415-1392-4a80-81d8-d6bab6125e02/original.f07d1e26-f9f0-4563-b201-09588a64ed19_patch.pdf"
    files = not_patch
    failed_key = []
    patch_metadata_s3(file_url, patch, failed_key)
